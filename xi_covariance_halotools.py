import numpy as np
from halotools.mock_observables import tpcf_jackknife
from astropy.table import Table

if __name__ == "__main__":
    filename = "gals_mock_bolshoi_z00_sigmaM_43_massMin_1200_alpha115_mass0_1102_mass1_1301_fcentral_100.csv"
    xi_filename = "xi_jack_mock_bolshoi_z00_sigmaM_43_massMin_1200_alpha115_mass0_1102_mass1_1301_fcentral_100.csv"
    cov_filename = "cov_jack_mock_bolshoi_z00_sigmaM_43_massMin_1200_alpha115_mass0_1102_mass1_1301_fcentral_100.csv"
    fsize = 0.15 # fraction of sample to use
    random_factor = 15 # how much bigger random sample is
    Lbox = 250

    table = Table.read(filename)
    sample_size = len(table)
    downsampling_mask = np.random.choice(np.arange(sample_size), size=int(fsize*sample_size), replace=False)
    random_size = int(sample_size * fsize * random_factor)

    data = np.c_[table["x"][downsampling_mask], table["y"][downsampling_mask], table["z"][downsampling_mask]]
    random = np.random.uniform(0, Lbox, (random_size, 3))

    r = np.logspace(-1, np.log10(50), 20)
    #r = np.logspace(0, np.log10(30), 20)
    #r = np.logspace(-2, np.log10(50), 50)

    xi_jack, cov_jack = tpcf_jackknife(data, random, r, Nsub=[5,5,5],
                                       num_threads="max", period=Lbox,
                                       estimator="Landy-Szalay", seed=0)
    r_mean = 0.5*(r[:-1] + r[1:])
    err = np.sqrt(np.diagonal(cov_jack))
    np.savetxt(xi_filename, np.c_[r_mean, xi_jack, err])
    np.savetxt(cov_filename, cov_jack.reshape(cov_jack.size, 1))

