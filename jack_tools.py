import numpy as np
import treecorr
from itertools import product


def calculate_xi_wrapper(catalog, rand_catalog, min_sep=1e-2, max_sep=40, nbins=40):
    """
    Wraps treecorr to calculate spatial correlation function xi.

    catalog: np.ndarray
        array with shape (Ngal, 3) whose columns contain x, y and z of galaxies, in Mpc/h
    rand_catalog: np.ndarray
        same as catalog, but for random sample
    min_sep: float
        minimum value of interval to calculate xi
    max_sep: float
        maximum value of interval to calculate xi
    nbins: int
        number of bins of interval to calculate xi

    returns:
        r, xi
        r: np.ndarray
            array with position of mean point of bins used to calculate xi
        xi: np.array
            array with calculated values of xi
    """
    Ngals, dgals = catalog.shape
    Nrand, drand = catalog.shape
    if dgals != 3:
        raise ValueError("catalog array shape should be (N, 3)")
    if drand != 3:
        raise ValueError("rand_catalog array shape should be (N, 3)")
    if Ngals == 0:
        raise ValueError("No galaxies in catalog array!")
    if Nrand == 0:
        raise ValueError("No randoms in rand_catalog array!")

    gg = treecorr.NNCorrelation(min_sep=min_sep, max_sep=max_sep, nbins=nbins)
    gr = treecorr.NNCorrelation(min_sep=min_sep, max_sep=max_sep, nbins=nbins)
    rr = treecorr.NNCorrelation(min_sep=min_sep, max_sep=max_sep, nbins=nbins)

    tc_catalog = treecorr.Catalog(x=catalog[:,0], y=catalog[:,1], z=catalog[:,2])
    tc_rand_catalog = treecorr.Catalog(x=rand_catalog[:,0], y=rand_catalog[:,1], z=rand_catalog[:,2])

    gg.process(tc_catalog)
    gr.process(tc_catalog, tc_rand_catalog)
    rr.process(tc_rand_catalog)

    xi, _ = gg.calculateXi(rr, gr)
    r = np.exp(gg.meanlogr)

    return r, xi


def calculate_jackknife_samples(function, catalog, rand_catalog, Lbox, n_jack_subdivision=5, **kwargs):
    """
    Calculate function on jackknife subsamples.

    function: callable object
        function, receiving as parameters catalog, rand_catalog and **kwargs, to be calculated on jackknife subsamples.
    catalog: np.ndarray
        array with shape (Ngal, 3) whose columns contain x, y and z of galaxies, in Mpc/h
    rand_catalog: np.ndarray
        same as catalog, but for random sample
    Lbox: int of array-like
        size of simulated box
    n_jack_subdivision: int or array-like [x_sub, y_sub, z_sub]
        number of subdivisions along each dimension to make.
    **kwargs:
        other keyword args to pass to function

    returns:
        f_values
        f_values: np.ndarray
            array with value of f calculated on jackknife samples
    """
    if type(Lbox) == int:
        Lbox = np.array([Lbox, Lbox, Lbox])
    if type(n_jack_subdivision) == int:
        n_jack_subdivision = np.array([n_jack_subdivision, n_jack_subdivision, n_jack_subdivision])

    xbins = np.linspace(0, Lbox[0], n_jack_subdivision[0], endpoint=False)
    ybins = np.linspace(0, Lbox[1], n_jack_subdivision[1], endpoint=False)
    zbins = np.linspace(0, Lbox[2], n_jack_subdivision[2], endpoint=False)
    subsample_vertices = np.array( [ [np.array(p), np.array(p) + Lbox/n_jack_subdivision]
                                     for p in product(xbins, ybins, zbins) ] )

    f_values = []
    n_jack = subsample_vertices.shape[0]
    for i in range(n_jack):
        print("Jack subsample %d" % i)
        subsample_low, subsample_high = subsample_vertices[i,0,:], subsample_vertices[i,1,:]
        cat_mask = (catalog < subsample_low).any(axis=1) | (catalog >= subsample_high).any(axis=1)

        rand_cat_mask = (rand_catalog < subsample_low).any(axis=1) | (rand_catalog >= subsample_high).any(axis=1)

        f_value = function(catalog[cat_mask], rand_catalog[rand_cat_mask],
                           **kwargs)
        f_values.append(f_value)

    f_values = np.array(f_values)

    return f_values


def calculate_jackknife_xi_covariance(catalog, rand_catalog, min_sep, max_sep, nbins, Lbox, n_jack_subdivision=5):
    """
    Calculate spatial correlation function xi and its covariance matrix with treecorr.

    catalog: np.ndarray
        array with shape (Ngal, 3) whose columns contain x, y and z of galaxies, in Mpc/h
    rand_catalog: np.ndarray
        same as catalog, but for random sample
    min_sep: float
        minimum value of interval to calculate xi
    max_sep: float
        maximum value of interval to calculate xi
    Lbox: int of array-like
        size of simulated box
    nbins: int
        number of bins of interval to calculate xi
    n_jack_subdivision: int or array-like [x_sub, y_sub, z_sub]
        number of subdivisions along each dimension to make.

    returns:
        r, xi, cov
        r: np.ndarray
            array with position of mean point of bins used to calculate xi
        xi: np.array
            array with calculated values of xi
        cov: covariance matrix of xi
    """
    if type(Lbox) == int:
        Lbox = np.array([Lbox, Lbox, Lbox])
    if type(n_jack_subdivision) == int:
        n_jack_subdivision = np.array([n_jack_subdivision, n_jack_subdivision, n_jack_subdivision])

    xbins = np.linspace(0, Lbox[0], n_jack_subdivision[0], endpoint=False)
    ybins = np.linspace(0, Lbox[1], n_jack_subdivision[1], endpoint=False)
    zbins = np.linspace(0, Lbox[2], n_jack_subdivision[2], endpoint=False)
    subsample_vertices = np.array( [ [np.array(p), np.array(p) + Lbox/n_jack_subdivision]
                                     for p in product(xbins, ybins, zbins) ] )

    xis_jack = []
    n_jack = subsample_vertices.shape[0]
    for i in range(n_jack):
        print("Jack subsample %d" % i)
        subsample_low, subsample_high = subsample_vertices[i,0,:], subsample_vertices[i,1,:]
        cat_mask = (catalog < subsample_low).any(axis=1) | (catalog >= subsample_high).any(axis=1)

        rand_cat_mask = (rand_catalog < subsample_low).any(axis=1) | (rand_catalog >= subsample_high).any(axis=1)

        r_jack, xi_jack = calculate_xi_wrapper(catalog[cat_mask], rand_catalog[rand_cat_mask],
                                               min_sep=min_sep, max_sep=max_sep, nbins=nbins)
        xis_jack.append(xi_jack)

    xis_jack = np.array(xis_jack)
    xi_jack_mean = xis_jack.mean(axis=0)
    xis_jack_dev = xis_jack - xi_jack_mean
    cov_jack = (n_jack - 1)/n_jack * np.dot(xis_jack_dev.T, xis_jack_dev)

    return r_jack, xi_jack_mean, cov_jack


def calculate_jackknife_xi_covariance2(catalog, rand_catalog, min_sep, max_sep, nbins, Lbox, n_jack_subdivision=5):
    """
    Calculate spatial correlation function xi and its covariance matrix with treecorr.

    catalog: np.ndarray
        array with shape (Ngal, 3) whose columns contain x, y and z of galaxies, in Mpc/h
    rand_catalog: np.ndarray
        same as catalog, but for random sample
    min_sep: float
        minimum value of interval to calculate xi
    max_sep: float
        maximum value of interval to calculate xi
    Lbox: int of array-like
        size of simulated box
    nbins: int
        number of bins of interval to calculate xi
    n_jack_subdivision: int or array-like [x_sub, y_sub, z_sub]
        number of subdivisions along each dimension to make.

    returns:
        r, xi, cov
        r: np.ndarray
            array with position of mean point of bins used to calculate xi
        xi: np.array
            array with calculated values of xi
        cov: covariance matrix of xi
    """
    n_jack = n_jack_subdivision ** 3
    def calculate_xi_wrapper_only_xi(*args, **kwargs):
        _, xi = calculate_xi_wrapper(*args, **kwargs)
        return xi
    xis_jack = calculate_jackknife_samples(calculate_xi_wrapper_only_xi, catalog, rand_catalog,
                                           Lbox, n_jack_subdivision,
                                           min_sep=min_sep, max_sep=max_sep, nbins=nbins)
    xi_jack_mean = xis_jack.mean(axis=0)
    xis_jack_dev = xis_jack - xi_jack_mean
    cov_jack = (n_jack - 1.0)/n_jack * np.dot(xis_jack_dev.T, xis_jack_dev)

    r_jack, xi_all = calculate_xi_wrapper(catalog, rand_catalog,
                                          min_sep=min_sep, max_sep=max_sep, nbins=nbins)

    return r_jack, xi_jack_mean, cov_jack


def calculate_jackknife_galaxy_density(catalog, Lbox, n_jack_subdivision=5):
    """
    Calculates galaxy density for catalog on volume.

    catalog: np.ndarray
        array with shape (Ngal, 3) whose columns contain x, y and z of galaxies, in Mpc/h
    Lbox: int of array-like
        size of simulated box
    n_jack_subdivision: int or array-like [x_sub, y_sub, z_sub]
        number of subdivisions along each dimension to make.

    returns:
        galaxy_density: float
            galaxy density, in h^3 Mpc^{-3}
    """
    try:
        Ngal, ndim = catalog.shape
        assert ndim == 3
    except (ValueError, AssertionError):
        raise ValueError("catalog shape should be # of galaxies, 3.")
    except AttributeError:
        raise ValueError("catalog should be numpy array.")

    volume = Lbox ** 3 -  (Lbox / n_jack_subdivision) ** 3
    def galaxy_density(catalog, rand_catalog):
        Ngal, _ = catalog.shape
        return Ngal / volume

    rand_catalog = catalog.copy()
    jack_densities = calculate_jackknife_samples(galaxy_density, catalog, rand_catalog, Lbox, n_jack_subdivision)

    n_jack = len(jack_densities)
    mean_density = jack_densities.mean()
    var_density = (n_jack - 1)/n_jack * np.sum((jack_densities - mean_density)**2)
    return mean_density, var_density

